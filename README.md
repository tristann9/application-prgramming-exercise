# Application Programming Exercise

An exercise that demonstrates an implementation for reading from a datasource a set of locations and the routes between them.

The exercise implements reading from an Excel file which contains three columns:

* From
* To
* Transit Time

From these three columns it is able to build a set of locations, connections between them, and the time it takes from one location to another.

The exercise is written in a way that alternative datasources can be created via a service provider interface.


## Clone, Build and Run

* <code>git clone https://bitbucket.org/tristann9/application-prgramming-exercise.git</code>
* <code>cd application-prgramming-exercise</code>
* <code>mvn clean package</code>
* <code>cd cli-execute</code>
* Run the cli-execute application:  <code>java -jar cli-execute.jar</code>    (NOTE: Java 8 is required)

## Use the API Factory
The ExampleApiFactory class is the main api class for creating datasources, location/route queries, and graph searching.

Some examples are:

* Create the Exercise instance.
<code>IExerciseApi api = ExerciseApiFactory.get();</code>

* Get a list of all locations, sorted by name ascending.
<code>api.createLocationQuery().orderBy(ILocation.PROPERTY.NAME).getResults()</code>

* Get a list of locations with the name "Dublin".  This will return a list with only one result.
<code>api.createLocationQuery().hasName("Dublin".getResults()</code>

* Get a list containing "Dublin" and "Zurich".
<code>q.hasName("Dublin").hasName("Zurich”).operator(OR).getResults()</code>

* Get a list of locations with no neighbours and sort the list descending.
<code>api.createLocationQuery().setIsolated(true).orderBy(ILocation.PROPERTY.NAME,false).getResults()</code>

* Get a list of locations that originate from Dublin
<code>api.createRouteQuery().setFromLocation(dublinLocation).getResults()</code>

* Get a list of locations that arrive at Dublin
<code>api.createRouteQuery().setToLocation(dublinLocation).getResults()</code>

* Get the quickest path between Dublin and Zurich
<code>api.createRouteQuery().getRoutesBetween(dublinLocation,zurichLocation)</code>

## Main Modules:

### libcommon

* A collection of interfaces that specify how a datasource and queries should be implemented.
* Also contains a service locator to find implemented datasources and an API factory.
* Implements a modified dijkstra's algorithm using a primary queue to find the quickest route when provided two locations.
* ExerciseApiFactory is the central factory for creating a datasource, query builders, and graph searching.

### ds-beans-java8
* An implementation datasource to read Excel files (via Apache POI) and query the data (Java Collections, streams, lambda).
* Compatible with Java 8 or higher.

### cli-execute
A simple self-looping command line application that can be used to:

* Print all locations
* Print all locations (in reverse order)
* Print all locations with no neighbours (isolated)
* Print all routes
* Load a new Excel file
* Find all outgoing routes given a location
* Find all incoming routes given a location
* Find the quickest path given two locations

The cli-execute application uses whatever datasource is implemented and picked up by the service locator in libcommon.
Using a different datasource implementation is a matter of:
 
 * Changing the dependency artifact in cli-execute/pom.xml from ds-beans-java8 to something else.
 * Run: <code>mvn clean package</code>
 * Run: <code>java -jar cli-execute.jar</code>

## Dummy Modules:

### ds-beans-java6
A placeholder for a datasource that reads Excel file, but compatible with Java 6 or higher.


### ds-jpa-excel
A placeholder for a datasource to read Excel files through JPA (DataNucleus). Other possible implementations can be Hibernate, EclipseLink, etc.


### webapp
A placeholder for a web front end that interacts with an implemented datasource


## 3rd Party Libraries

 * JUnit - A unit testing framework for the Java programming language
 * Mockito - A framework that allows to create mock objects, used mainly in unit testing.
 * equalsverifier - A unit testing utility to help verify whether the contract for the equals and hashCode methods in a class are met.
 * logback - An implementation of the SLF4J API that is used for logging.
 * groovy - A dynamic language with features similar to those of Python, Ruby, Perl, and Smalltalk, but for the Java Virtual Machine.  Groovy is used to configure logback logging.
 * poi - A Java library for reading and writing files in Microsoft Office formats, such as Word, PowerPoint and Excel.
 * commons-lang3 - Helper utilities for the java.lang API, notably String manipulation methods, basic numerical methods, object reflection, concurrency, creation and serialisation, System properties and utilities to help with building methods, such as hashCode, toString and equals.
 * jipsy - Configurable Java Annotation Processor to simplify the use of the Service Provider Interface
 * lz4 -- An implementation of the xxHash algorithm to create mock IDs for the Location and Route Beans.  An alternative to using hashCodes.