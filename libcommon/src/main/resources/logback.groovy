// LogBack configuration file for enabling logging in LogCombo example application

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy

import static ch.qos.logback.classic.Level.DEBUG
import static ch.qos.logback.classic.Level.ERROR

def logDir = "target" + File.separator + "logs" + File.separator
def baseName = "ProgrammingExercise"

// Invoking the scan() method instructs logback to periodically scan the logback.groovy file for changes( default = 1min).
scan("120 seconds")

// Standard ouput (console) log
appender("CONSOLE", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} %-5level [%thread][%logger{0}] %m%n"
    }

}

appender("FILE", RollingFileAppender) {
    file = "${logDir}${baseName}.log"
    rollingPolicy(FixedWindowRollingPolicy) {
        fileNamePattern = "${logDir}${baseName}.%i.log.zip"
        minIndex = 1
        maxIndex = 3
    }
    triggeringPolicy(SizeBasedTriggeringPolicy) {
        maxFileSize = "1MB"
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} %-5level [%thread][%logger{0}] %m%n"
    }
}


root(ERROR, ["CONSOLE", "FILE"])

logger("com.armacsys.demo", DEBUG)


