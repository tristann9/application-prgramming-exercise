package com.armacsys.demo.util;

import com.armacsys.demo.exceptions.DemoFeatureNotImplementedException;
import com.armacsys.demo.model.ILocation;
import com.armacsys.demo.model.IRoute;

import java.util.List;

/**
 * Created by teveritt on 07/11/2014.
 */
public interface IRouteQuery extends IGeneralQuery<IRouteQuery, IRoute, IRoute.PROPERTY> {

    /**
     * An alias call for the setFromLocation(ILocation), but with the name string only.
     *
     * @param locationName -- The name of a location
     */
    public IRouteQuery setFromLocation(String locationName);

    /**
     * Specify the starting location of a route to find all outgoing routes.
     *
     * @param location -- The location to search outgoing routes.
     */
    public IRouteQuery setFromLocation(ILocation location);

    /**
     * An alias call for the setToLocation(ILocation), but with the name string only.
     *
     * @param locationName -- The name of a location
     */
    public IRouteQuery setToLocation(String locationName);

    /**
     * Specify the ending location of a route to find all incoming routes.
     *
     * @param location -- The location to search incoming routes.
     */
    public IRouteQuery setToLocation(ILocation location);

    /**
     * Return routes that contain the specified transitTime
     *
     * @param transitTime -- The value for transitTime
     */
    public IRouteQuery hasTransitTime(int transitTime);

    /**
     * Return routes that contain transitTime less than the given value
     *
     * @param value -- The upper bound value for transitTime
     */
    public IRouteQuery hasTransitTimeGreaterThan(int value);

    /**
     * Return routes that contain transitTime higher than the given value
     *
     * @param value -- The lower bound value for transitTime
     */
    public IRouteQuery hasTransitTimeLessThan(int value);

    /**
     * Limit the maximum connections when searching for the quickest route.
     * In other words, if the max connection is 2 and the quickest route has 3, it will
     * return saying it could not find any valid routes.
     *
     * @param limit -- The upper bound value for the number of connections in getPathBetween
     */
    public IRouteQuery setMaxConnections(int limit) throws DemoFeatureNotImplementedException;

    /**
     * An alias call to the IGraphSearch get shortest route.
     *
     * @param fromLocation -- The starting point
     * @param toLocation -- The finishing point
     * @return A list of locations showing the quickest path.
     */
    public List<ILocation> getPathBetween(ILocation fromLocation, ILocation toLocation);

    /**
     * Similar to getPathBetween, but instead of returning a list of path this one will
     * return a list of the routes involved.
     *
     * @param fromLocation -- The starting point
     * @param toLocation -- The finishing point
     */
    public List<IRoute> getRoutesBetween(ILocation fromLocation, ILocation toLocation);

}
