package com.armacsys.demo.util;

import com.armacsys.demo.exceptions.DemoFeatureNotImplementedException;

import java.util.List;
import java.util.Set;

/**
 * Created by teveritt on 08/11/2014.
 */
public interface IGeneralQuery<S, T, E extends Enum<E>> {

    public S hasId(Integer... ids);

    /**
     * Sets the limit of values to return when calling getResults
     *
     * @param limit -- The limit of returning results.
     */
    public S setResultLimit(int limit) throws DemoFeatureNotImplementedException;

    /**
     * Alias for the orderBy(property,ascending) method, but with default sorting.
     *
     * @param property -- The property to sort by
     */
    public S orderBy(E property);

    /**
     * Sort by the given property either ascending (true) or descending (false).
     *
     * @param property -- The property to sort by
     * @param ascending -- ascending or descending.
     */
    public S orderBy(E property, boolean ascending);

    /**
     * Reset the query.
     *
     */
    public void reset();

    /**
     * Return the count of the result set.
     *
     */
    public int getCount();

    /**
     * A convenience method for creating a new instance of this query along with all it's conditions.
     *
     */
    public S copyQuery() throws DemoFeatureNotImplementedException;

    /**
     * Return the results of the query given the conditions set.
     *
     */
    public Set<T> getResults();

    /**
     * Specify the operator to apply on all the conditions.
     * For example, AND/OR
     *
     * @param operator -- The operator to apply
     */
    public S operator(LOGICAL_OPERATOR operator);


    public LOGICAL_OPERATOR getOperator();

    public enum LOGICAL_OPERATOR {
        AND, OR
    }
}
