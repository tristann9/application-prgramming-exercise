package com.armacsys.demo.util;

import com.armacsys.demo.model.ILocation;

/**
 * Created by teveritt on 07/11/2014.
 */
public interface ILocationQuery extends IGeneralQuery<ILocationQuery, ILocation, ILocation.PROPERTY> {

    /**
     * Specifies the name of a location to query for.
     * Note: if multiple names are used the the operand must be set to OR
     * so as to get any results back
     *
     * @param names -- One or more names
     */
    public ILocationQuery hasName(String... names);

    /**
     * Instruct the query to only search for nodes that don't have any outgoing or incoming
     * routes.
     *
     * @param isIsolated -- To search for isolated locations or not.
     */
    public ILocationQuery setIsolated(boolean isIsolated);

    public boolean isIsolated();

}
