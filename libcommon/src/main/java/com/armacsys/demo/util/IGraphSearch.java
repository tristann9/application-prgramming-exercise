package com.armacsys.demo.util;

import com.armacsys.demo.model.ILocation;

import java.util.List;

/**
 * Created by teveritt on November 09, 2014.
 */
public interface IGraphSearch {

    /**
     * Finds the quickest route between two locations.
     *
     * @param startLocation -- The starting point
     * @param endLocation -- The finishing point
     */
    public List<ILocation> getShortestRoute(ILocation startLocation, ILocation endLocation);
}
