package com.armacsys.demo.util;

import com.armacsys.demo.data.IDataSource;
import com.armacsys.demo.lib.i18n.I18N;
import com.armacsys.demo.lib.i18n.MsgKey;
import com.armacsys.demo.model.ILocation;
import com.armacsys.demo.model.IRoute;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by teveritt on 08/11/2014.
 *
 * A modified version of the Dijkstra's algorithm so that it uses a priority queue
 * and queries the datasource everytime it needs to find the outgoing routes of a location
 *
 * https://en.wikipedia.org/wiki/Dijkstra's_algorithm#Using_a_priority_queue
 */
public class DijkstraModifiedSearch implements IGraphSearch {
    private static final Logger logger = LoggerFactory
            .getLogger(DijkstraModifiedSearch.class);

    private final IDataSource ds;
    private final Queue<LocationWrapper> queue;
    private final Set<LocationWrapper> processed;
    private final Map<ILocation, ILocation> breadcrumbs;
    private final Map<ILocation, Integer> weightCache;

    public DijkstraModifiedSearch(IDataSource ds) {
        this.ds = ds;
        queue = new PriorityQueue<>(ds.createLocationQuery().getCount());
        processed = new HashSet<>();
        breadcrumbs = new HashMap<>();
        weightCache = new HashMap<>();
    }

    public List<ILocation> getShortestRoute(ILocation startLocation, ILocation endLocation) {
        logger.trace(I18N.msg(MsgKey.GEN_ENTERING) + " getShortestRoute(startLocation={},endLocation={})", startLocation, endLocation);
        long duration = System.currentTimeMillis();

        logger.debug(I18N.msg(MsgKey.UTIL_SEARCHING_ROUTE_PATH), startLocation, endLocation);

        // Run a modified Dijkstra's algorithm based on priority queue.
        // When searching for neighbours, it pulls from the datasource again.
        queue.add(new LocationWrapper(startLocation, 0));
        while (!queue.isEmpty()) {
            LocationWrapper start = queue.remove();
            processed.add(start);

            if (start.getLocation().equals(endLocation)) {
                logger.debug(I18N.msg(MsgKey.UTIL_SKIPPING_STARTING_LOC), start);
                continue;
            }

            logger.debug(I18N.msg(MsgKey.UTIL_START_EVALUATING), start);


            Set<IRoute> routes = ds.createRouteQuery().setFromLocation(start.getLocation()).getResults();
            logger.debug(I18N.msg(MsgKey.UTIL_FOUND_DESTINATIONS), routes.size());//
            for (IRoute route : routes) {
                LocationWrapper destination = new LocationWrapper(route.getToLocation());

                if (!processed.contains(destination)) {
                    logger.debug(I18N.msg(MsgKey.UTIL_PROCESSING_DEST), destination);

                    int routeTransitTime = route.getTransitTime();
                    int weight = start.getWeight() + routeTransitTime;
                    if (destination.getWeight() > weight) {
                        destination.setWeight(weight);
                        logger.debug(I18N.msg(MsgKey.UTIL_ADDING_DEST_TO_Q), destination);
                        queue.add(destination);
                        breadcrumbs.put(destination.getLocation(), start.getLocation());
                        weightCache.put(destination.getLocation(), weight);
                    }
                }
            }
        }


        LinkedList<ILocation> path = new LinkedList<>();
        if (!breadcrumbs.containsKey(endLocation)) {
            return path; // no path found
        }
        logger.debug(I18N.msg(MsgKey.UTIL_BUILDING_PATH));

        ILocation lastBreadcrumb = endLocation;
        path.addFirst(lastBreadcrumb);
        // work backwards through the breadcrumbs.
        while (breadcrumbs.containsKey(lastBreadcrumb)) {
            lastBreadcrumb = breadcrumbs.get(lastBreadcrumb);
            path.addFirst(lastBreadcrumb);
        }

        duration = System.currentTimeMillis() - duration;
        String durationStr = DurationFormatUtils.formatDurationWords(duration, true, true);
        logger.trace(I18N.msg(MsgKey.GEN_METHOD_TOOK), "getShortestRoute", duration, durationStr);
        logger.trace(I18N.msg(MsgKey.GEN_LEAVING) + " getShortestRoute():{}", path);
        return path;
    }

    private class LocationWrapper implements Comparable<LocationWrapper> {
        final private ILocation location;
        private int weight = Integer.MAX_VALUE;

        private LocationWrapper(ILocation location, int weight) {
            this.location = location;
            this.weight = weight;
        }

        private LocationWrapper(ILocation location) {
            this.location = location;
            if (weightCache.containsKey(location)) {
                this.weight = weightCache.get(location);
            }
        }

        public ILocation getLocation() {
            return this.location;
        }

        public int getWeight() {
            return this.weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        @Override
        public int compareTo(LocationWrapper that) {
            if (this.getWeight() > that.getWeight()) {
                return 1;
            } else {
                return -1;
            }
        }

        @Override
        final public int hashCode() {
            return this.location.getId().hashCode();
        }

        @Override
        final public boolean equals(Object o) {
            if (o instanceof LocationWrapper) {
                return this.location.getId().equals(((LocationWrapper) o).getLocation().getId());
            } else if (o instanceof ILocation) {
                return this.location.getId().equals(((ILocation) o).getId());
            } else {
                return false;
            }
        }

        @Override
        public String toString() {
            return String.format("%s{" +
                            "id='%s', name='%s', weight='%s'}", getClass().getSimpleName(), location.getId(),
                    location.getName(), weight);
        }

    }
}
