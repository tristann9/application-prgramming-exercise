package com.armacsys.demo.lib.i18n;

/**
 * Created by teveritt on November 09, 2014.
 */
public enum BundleType {
    ERRORS(1),
    MESSAGES(2);

    private int value;

    BundleType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        switch (value) {
            case 1:
                return "LibErrors";
            case 2:
            default:
                return "LibMessages";
        }
    }
}