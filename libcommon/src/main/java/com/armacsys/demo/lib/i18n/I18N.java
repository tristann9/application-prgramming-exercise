package com.armacsys.demo.lib.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by teveritt on November 09, 2014.
 */
public final class I18N {

    public static String msg(MsgKey key) {
        return bundle(BundleType.MESSAGES).getString(key.toString());
    }

    public static String msg(MsgKey key, Locale locale) {
        return bundle(BundleType.MESSAGES, locale).getString(key.toString());
    }

    public static String err(MsgKey key) {
        return bundle(BundleType.ERRORS).getString(key.toString());
    }

    public static String err(MsgKey key, Locale locale) {
        return bundle(BundleType.ERRORS, locale).getString(key.toString());
    }

    public static ResourceBundle bundle(BundleType type) {
        return ResourceBundle.getBundle(type.toString());
    }

    public static ResourceBundle bundle(BundleType type, Locale locale) {
        return ResourceBundle.getBundle(type.toString(), locale);
    }
}
