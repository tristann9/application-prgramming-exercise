package com.armacsys.demo;

import com.armacsys.demo.data.IDataSource;
import com.armacsys.demo.util.ILocationQuery;
import com.armacsys.demo.util.IRouteQuery;

/**
 * Created by teveritt on November 10, 2014.
 *
 * An interface representing the ExerciseApiFactory.
 */
public interface IExerciseApi {

    /**
     * Returns a new {@link com.armacsys.demo.data.IDataSource} instance.
     */
    public IDataSource getDataSource();

    /**
     * Returns a new {@link com.armacsys.demo.util.ILocationQuery} instance.
     */
    public ILocationQuery createLocationQuery();

    /**
     * Returns a new {@link com.armacsys.demo.util.IRouteQuery} instance.
     */
    public IRouteQuery createRouteQuery();
}
