package com.armacsys.demo;

import com.armacsys.demo.data.IDataSource;
import com.armacsys.demo.data.spi.IServiceProvider;
import com.armacsys.demo.lib.i18n.I18N;
import com.armacsys.demo.lib.i18n.MsgKey;
import com.armacsys.demo.util.ILocationQuery;
import com.armacsys.demo.util.IRouteQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ServiceLoader;

/**
 * Created by teveritt on 06/11/2014.
 */
public class ExerciseApiFactory implements IExerciseApi {
    private static final Logger logger = LoggerFactory
            .getLogger(ExerciseApiFactory.class);
    // Make the spi a singleton so that it does not
    // call the ServiceLoader on every new instance.
    private static IServiceProvider spi = null;
    private final IDataSource ds;

    private ExerciseApiFactory() {
        // There should only be one running serviceProvider.
        if (spi == null) {
            spi = createService(IServiceProvider.class);
        }

        ds = spi.createDataSource();
    }

    public static IExerciseApi get() {
        return new ExerciseApiFactory();
    }

    public IDataSource getDataSource() {
        return ds;
    }

    public ILocationQuery createLocationQuery() {
        return getDataSource().createLocationQuery();
    }

    public IRouteQuery createRouteQuery() {
        return getDataSource().createRouteQuery();
    }

    private <T> T createService(final Class<T> clazz) {
        T service = null;
        ServiceLoader<T> loader = ServiceLoader.load(clazz);
        if (loader.iterator().hasNext()) {
            service = loader.iterator().next();
            logger.debug(I18N.msg(MsgKey.LOADER_USING_IMPL),
                    clazz.getSimpleName(), service.getClass().getName());
        } else {
            logger.debug(I18N.msg(MsgKey.LOADER_NO_IMPL_FOUND),
                    clazz.getSimpleName());
        }
        return service;
    }
}
