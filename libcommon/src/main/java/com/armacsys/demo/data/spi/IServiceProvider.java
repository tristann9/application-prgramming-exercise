package com.armacsys.demo.data.spi;

import com.armacsys.demo.data.IDataSource;

/**
 * Created by teveritt on 06/11/2014.
 *
 * An interface representing a service provider.  All implementing classes must append the
 * class level annotation @ServiceProviderFor(IServiceProvider.class) to be considered a
 * service provider.
 */
public interface IServiceProvider {
    /**
     * Returns a new {@link com.armacsys.demo.data.IDataSource} instance.
     */
    IDataSource createDataSource();

}
