package com.armacsys.demo.data;

import com.armacsys.demo.exceptions.DemoFeatureNotImplementedException;
import com.armacsys.demo.model.ILocation;
import com.armacsys.demo.model.IRoute;
import com.armacsys.demo.util.IGraphSearch;
import com.armacsys.demo.util.ILocationQuery;
import com.armacsys.demo.util.IRouteQuery;

/**
 * Created by teveritt on November 09, 2014.
 *
 * An interface that represents a datasouce for the API to reference when making queries.
 */
public interface IDataSource {

    /**
     * Creates a Location
     *
     * @param location -- the location bean/entity to create
     */
    public void createLocation(ILocation location) throws DemoFeatureNotImplementedException;

    /**
     * Deletes a Location
     *
     * @param location -- the location bean/entity to delete
     */
    public void deleteLocation(ILocation location) throws DemoFeatureNotImplementedException;


    /**
     * Creates a Route
     *
     * @param route -- the route bean/entity to create
     */
    public void createRoute(IRoute route) throws DemoFeatureNotImplementedException;

    /**
     * Deletes a Route -- the route bean/entity to delete
     *
     * @param route
     */
    public void deleteRoute(IRoute route) throws DemoFeatureNotImplementedException;


    /**
     * Create a Location Query to query the datasource for locations
     *
     * @return ILocationQuery query
     */
    public ILocationQuery createLocationQuery();

    /**
     * Create a Route Query to query the datasource for routes
     *
     * @return IRouteQuery query
     */
    public IRouteQuery createRouteQuery();

    /**
     * Create a Graph Search to query two locations in a set
     * of locations and routes between them.
     *
     * @return IRouteQuery query
     */
    public IGraphSearch createGraphSearch();


}
