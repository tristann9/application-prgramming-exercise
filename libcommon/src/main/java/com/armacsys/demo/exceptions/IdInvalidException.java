package com.armacsys.demo.exceptions;

import com.armacsys.demo.lib.i18n.I18N;
import com.armacsys.demo.lib.i18n.MsgKey;

/**
 * Created by teveritt on 05/11/2014.
 */
public class IdInvalidException extends IllegalArgumentException {


    private static final long serialVersionUID = 1027651193774645328L;

    public IdInvalidException() {
        super(I18N.err(MsgKey.ERR_INVALID_ID));
    }
}
