package com.armacsys.demo.exceptions;

/**
 * Created by teveritt on 05/11/2014.
 */
public class RouteServiceException extends Exception {

    private static final long serialVersionUID = 5979060531421016836L;

    public RouteServiceException() {

    }

    public RouteServiceException(String message) {
        super(message);
    }
}
