package com.armacsys.demo.exceptions;

/**
 * Created by teveritt on 05/11/2014.
 */
public class DemoFeatureNotImplementedException extends Exception {


    private static final long serialVersionUID = -6279575659974974549L;

    public DemoFeatureNotImplementedException() {

    }

    public DemoFeatureNotImplementedException(String message) {
        super(message);
    }
}
