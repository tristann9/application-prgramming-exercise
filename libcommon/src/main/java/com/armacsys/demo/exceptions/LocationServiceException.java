package com.armacsys.demo.exceptions;

/**
 * Created by teveritt on 05/11/2014.
 */
public class LocationServiceException extends Exception {

    private static final long serialVersionUID = 4097889090197589214L;

    public LocationServiceException() {

    }

    public LocationServiceException(String message) {
        super(message);
    }
}
