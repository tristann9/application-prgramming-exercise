package com.armacsys.demo.exceptions;

import com.armacsys.demo.lib.i18n.I18N;
import com.armacsys.demo.lib.i18n.MsgKey;

/**
 * Created by teveritt on 05/11/2014.
 */
public class IdAlreadySetException extends IllegalArgumentException {


    private static final long serialVersionUID = 1853938424817584414L;

    public IdAlreadySetException() {
        super(I18N.err(MsgKey.ERR_ID_ALREADY_SET));
    }
}
