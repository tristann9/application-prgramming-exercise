package com.armacsys.demo.model;

import java.io.Serializable;

/**
 * Created by teveritt on 05/11/2014.
 *
 * A model representation of a location.
 */
public interface IRoute extends Serializable, Comparable<IRoute> {

    public ILocation getFromLocation();

    public void setFromLocation(ILocation from);

    public ILocation getToLocation();

    public void setToLocation(ILocation to);

    public Integer getId();

    public void setId(Integer id);

    public Integer getTransitTime();

    public void setTransitTime(Integer transitTime);

    public enum PROPERTY {
        ID, FROM, TO, TRANSIT_TIME
    }
}
