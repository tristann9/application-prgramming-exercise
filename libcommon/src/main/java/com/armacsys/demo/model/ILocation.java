package com.armacsys.demo.model;

import java.io.Serializable;

/**
 * Created by teveritt on 05/11/2014.
 *
 * A model representation of a location.
 */
public interface ILocation extends Serializable, Comparable<ILocation> {

    public Integer getId();

    public void setId(Integer id);

    public String getName();

    public void setName(String name);

    public enum PROPERTY {
        ID, NAME
    }
}
