package com.armacsys.demo.model;

import net.jpountz.xxhash.XXHashFactory;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by teveritt on November 09, 2014.
 */
abstract class AbstractBean {

    private static final int SEED = 0xF73088F4;

    protected int generateUniqueId(String name) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Name cannot be null or empty.");
        }

        // xxHash is fast, high quality non-cryptographic hash function.
        // This exercise module uses it to generate IDs for beans
        //  @url=http://code.google.com/p/xxhash/.

        XXHashFactory factory = XXHashFactory.fastestInstance();
        byte[] data = name.getBytes();

        int hash = factory.hash32().hash(data, 0, data.length, SEED);

        // The hash values are in the positive and negative space
        // However, for simplicity and to keep IDs positive the hash value
        // will be made positive.  This increases the chance of collision,
        // but for a small application it should be fine.
        // Basically instead of 1 in 4,294,967,295 chance of a collision it's now
        // twice as likely.
        // In practice, the ID is guaranteed to be unique and this method would not exist.

        return Math.abs(hash);
    }
}
