package com.armacsys.demo.model;

import com.armacsys.demo.exceptions.BlankNameException;
import com.armacsys.demo.exceptions.IdAlreadySetException;
import com.armacsys.demo.exceptions.IdInvalidException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by teveritt on 05/11/2014.
 */


public class LocationBeanImpl extends AbstractBean implements ILocation {


    private static final long serialVersionUID = 3418515274039352932L;
    private Integer id;
    private String name = null;

    public LocationBeanImpl() {

    }

    public LocationBeanImpl(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public LocationBeanImpl(String name) {
        this.id = generateUniqueId(name);
        this.name = name;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        if (this.id != null && !this.id.equals(id)) {
            throw new IdAlreadySetException();
        } else if (id == null || id <= 0) {
            throw new IdInvalidException();
        }


        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        if (StringUtils.isBlank(name)) {
            throw new BlankNameException();
        }
        this.name = name;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocationBeanImpl)) return false;

        LocationBeanImpl that = (LocationBeanImpl) o;

        return new EqualsBuilder().append(this.getId(), that.getId()).append(this.getName(), that.getName())
                .isEquals();
    }

    @Override
    public final int hashCode() {

        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
                append(getId()).append(getName()).toHashCode();
    }

    @Override
    public String toString() {
        return String.format("%s{" +
                "id='%s', name='%s'}", LocationBeanImpl.class.getSimpleName(), id, name);
    }

    @Override
    public int compareTo(ILocation that) {
        int name = this.getName().compareTo(that.getName());
        return name == 0 ? this.getId().compareTo(that.getId()) : name;
    }
}
