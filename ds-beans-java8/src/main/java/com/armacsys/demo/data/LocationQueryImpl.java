package com.armacsys.demo.data;

import com.armacsys.demo.exceptions.DemoFeatureNotImplementedException;
import com.armacsys.demo.lib.i18n.I18N;
import com.armacsys.demo.lib.i18n.MsgKey;
import com.armacsys.demo.model.ILocation;
import com.armacsys.demo.util.ILocationQuery;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by teveritt on 07/11/2014.
 */
public class LocationQueryImpl implements ILocationQuery {
    private static final Logger logger = LoggerFactory
            .getLogger(LocationQueryImpl.class);

    private final List<Predicate<ILocation>> predicateList = new LinkedList<>();
    private final PoiDataSourceImpl ds;
    private Comparator<ILocation> comparator = null;
    private LOGICAL_OPERATOR operator = null;
    private boolean isIsolated = false;

    LocationQueryImpl(PoiDataSourceImpl ds) {
        this.ds = ds;
        operator = LOGICAL_OPERATOR.AND;
    }

    @Override
    public ILocationQuery hasName(String... names) {
        Set<String> nameSet = new HashSet<>(Arrays.asList(names));
        Predicate<ILocation> predicate = location -> nameSet.contains(location.getName());

        predicateList.add(predicate);
        return this;
    }

    @Override
    public ILocationQuery hasId(Integer... ids) {
        Set<Integer> idSet = new HashSet<>(Arrays.asList(ids));
        Predicate<ILocation> predicate = location -> idSet.contains(location.getId());

        predicateList.add(predicate);
        return this;
    }

    public ILocationQuery setIsolated(boolean isIsolated) {
        this.isIsolated = isIsolated;
        return this;
    }

    public boolean isIsolated() {
        return isIsolated;
    }

    @Override
    public ILocationQuery setResultLimit(int limit) throws DemoFeatureNotImplementedException {
        throw new DemoFeatureNotImplementedException();
    }

    @Override
    public ILocationQuery orderBy(ILocation.PROPERTY property) {
        return orderBy(property, true);
    }

    @Override
    public ILocationQuery orderBy(ILocation.PROPERTY property, boolean ascending) {

        switch (property) {
            case ID:
                if (ascending) {
                    comparator = (e1, e2) -> e1
                            .getId().compareTo(e2.getId());
                } else {
                    comparator = (e1, e2) -> e2
                            .getId().compareTo(e1.getId());
                }

                break;
            case NAME:
                if (ascending) {
                    comparator = (e1, e2) -> e1
                            .getName().compareTo(e2.getName());
                } else {
                    comparator = (e1, e2) -> e2
                            .getName().compareTo(e1.getName());
                }

                break;
        }

        return this;
    }

    @Override
    public void reset() {
        predicateList.clear();
    }

    @Override
    public int getCount() {
        return getResults().size();
    }

    @Override
    public ILocationQuery copyQuery() throws DemoFeatureNotImplementedException {
        throw new DemoFeatureNotImplementedException();
    }

    @Override
    public ILocationQuery operator(LOGICAL_OPERATOR operator) {
        this.operator = operator;
        return this;
    }

    @Override
    public LOGICAL_OPERATOR getOperator() {
        return operator;
    }

    @Override
    public Set<ILocation> getResults() {
        logger.trace(I18N.msg(MsgKey.GEN_ENTERING) + " getResults(operator={})", operator);
        long duration = System.currentTimeMillis();


        BinaryOperator<Predicate<ILocation>> binOp = null;
        switch (operator) {
            case AND:
                binOp = Predicate::and;
                break;
            case OR:
                binOp = Predicate::or;
                break;
        }

        Supplier<LinkedHashSet<ILocation>> supplier = LinkedHashSet<ILocation>::new;
        Predicate<ILocation> predicates = predicateList.stream().reduce(binOp).orElse(x -> true);
        Stream<ILocation> stream;
        if (isIsolated()) {
            stream = ds.getOrphansCache().stream();
        } else {
            stream = ds.getLocationsCache().stream();
        }
        Set<ILocation> locations;
        if (comparator == null) {
            locations = stream.filter(predicates).collect(Collectors.toCollection(supplier));
        } else {
            locations = stream.sorted(comparator).filter(predicates).collect(Collectors.toCollection(supplier));
        }

        duration = System.currentTimeMillis() - duration;
        String durationStr = DurationFormatUtils.formatDurationWords(duration, true, true);
        logger.trace(I18N.msg(MsgKey.GEN_METHOD_TOOK), "getResults", duration, durationStr);
        logger.trace(I18N.msg(MsgKey.GEN_LEAVING) + " getResults(): {}", locations);
        return locations;
    }

    private Collection<ILocation> cache() {
        return ds.getLocationsCache();
    }
}
