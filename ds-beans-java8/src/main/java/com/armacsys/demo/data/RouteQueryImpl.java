package com.armacsys.demo.data;

import com.armacsys.demo.exceptions.DemoFeatureNotImplementedException;
import com.armacsys.demo.lib.i18n.I18N;
import com.armacsys.demo.lib.i18n.MsgKey;
import com.armacsys.demo.model.ILocation;
import com.armacsys.demo.model.IRoute;
import com.armacsys.demo.model.LocationBeanImpl;
import com.armacsys.demo.util.IRouteQuery;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by teveritt on 07/11/2014.
 */
public class RouteQueryImpl implements IRouteQuery {
    private static final Logger logger = LoggerFactory
            .getLogger(RouteQueryImpl.class);
    private List<Predicate<IRoute>> predicateList = null;
    private PoiDataSourceImpl ds = null;
    private Comparator<IRoute> comparator = null;
    private LOGICAL_OPERATOR operator = null;

    RouteQueryImpl(PoiDataSourceImpl ds) {
        this.ds = ds;
        predicateList = new LinkedList<>();
        operator = LOGICAL_OPERATOR.AND;
    }

    @Override
    public IRouteQuery hasId(Integer... ids) {
        Set<Integer> idSet = new HashSet<>(Arrays.asList(ids));
        Predicate<IRoute> predicate = route -> idSet.contains(route.getId());
        predicateList.add(predicate);
        return this;
    }

    @Override
    public IRouteQuery setFromLocation(String locationName) {
        return setFromLocation(new LocationBeanImpl(locationName));
    }

    @Override
    public IRouteQuery setFromLocation(ILocation location) {
        Predicate<IRoute> predicate = route -> route.getFromLocation().equals(location);
        predicateList.add(predicate);
        return this;
    }

    @Override
    public IRouteQuery setToLocation(String locationName) {
        return setToLocation(new LocationBeanImpl(locationName));
    }

    @Override
    public IRouteQuery setToLocation(ILocation location) {
        Predicate<IRoute> predicate = route -> route.getToLocation().equals(location);
        predicateList.add(predicate);
        return this;
    }

    @Override
    public IRouteQuery hasTransitTime(int transitTime) {
        Predicate<IRoute> predicate = route -> route.getTransitTime() == transitTime;
        predicateList.add(predicate);
        return this;
    }

    @Override
    public IRouteQuery hasTransitTimeGreaterThan(int value) {
        Predicate<IRoute> predicate = route -> route.getTransitTime() > value;
        predicateList.add(predicate);
        return this;
    }

    @Override
    public IRouteQuery hasTransitTimeLessThan(int value) {
        Predicate<IRoute> predicate = route -> route.getTransitTime() < value;
        predicateList.add(predicate);
        return this;
    }

    @Override
    public IRouteQuery orderBy(IRoute.PROPERTY property) {
        return orderBy(property, true);
    }

    @Override
    public IRouteQuery orderBy(IRoute.PROPERTY property, boolean ascending) {

        switch (property) {
            case ID:
                if (ascending) {
                    comparator = (e1, e2) -> e1
                            .getId().compareTo(e2.getId());
                } else {
                    comparator = (e1, e2) -> e2
                            .getId().compareTo(e1.getId());
                }

                break;
            case FROM:
                if (ascending) {
                    comparator = (e1, e2) -> e1
                            .getFromLocation().compareTo(e2.getFromLocation());
                } else {
                    comparator = (e1, e2) -> e2
                            .getFromLocation().compareTo(e1.getFromLocation());
                }

                break;
            case TO:
                if (ascending) {
                    comparator = (e1, e2) -> e1
                            .getToLocation().compareTo(e2.getToLocation());
                } else {
                    comparator = (e1, e2) -> e2
                            .getToLocation().compareTo(e1.getToLocation());
                }

                break;
            case TRANSIT_TIME:
                if (ascending) {
                    comparator = (e1, e2) -> e1
                            .getTransitTime().compareTo(e2.getTransitTime());
                } else {
                    comparator = (e1, e2) -> e2
                            .getTransitTime().compareTo(e1.getTransitTime());
                }

                break;
        }

        return this;
    }

    @Override
    public void reset() {
        predicateList.clear();
    }

    @Override
    public int getCount() {
        return getResults().size();
    }

    @Override
    public IRouteQuery setResultLimit(int limit) throws DemoFeatureNotImplementedException {
        throw new DemoFeatureNotImplementedException();
    }

    @Override
    public IRouteQuery setMaxConnections(int limit) throws DemoFeatureNotImplementedException {
        throw new DemoFeatureNotImplementedException();
    }

    @Override
    public List<ILocation> getPathBetween(ILocation fromLocation, ILocation toLocation) {
        return ds.createGraphSearch().getShortestRoute(fromLocation, toLocation);
    }

    @Override
    public List<IRoute> getRoutesBetween(ILocation fromLocation, ILocation toLocation) {
        List<ILocation> path = getPathBetween(fromLocation, toLocation);
        List<IRoute> routes = new LinkedList<>();
        if (path.size() > 1) {
            ILocation start = null;
            for (ILocation loc : path) {
                if (start == null) {
                    start = loc;
                    continue;
                }

                Collection<IRoute> r = ds.createRouteQuery().setFromLocation(start).setToLocation(loc).getResults();
                if (r.iterator().hasNext()) {
                    routes.add(r.iterator().next());
                }
                start = loc;
            }
        }

        return routes;

    }

    @Override
    public IRouteQuery copyQuery() throws DemoFeatureNotImplementedException {
        throw new DemoFeatureNotImplementedException();
    }

    @Override
    public Set<IRoute> getResults() {
        logger.trace(I18N.msg(MsgKey.GEN_ENTERING) + " getResults(operator={})", operator);
        long duration = System.currentTimeMillis();

        BinaryOperator<Predicate<IRoute>> binOp = null;
        switch (operator) {
            case AND:
                binOp = Predicate::and;
                break;
            case OR:
                binOp = Predicate::or;
                break;
        }

        Supplier<LinkedHashSet<IRoute>> supplier = LinkedHashSet<IRoute>::new;
        Predicate<IRoute> predicates = predicateList.stream().reduce(binOp).orElse(x -> true);
        Stream<IRoute> stream = ds.getRoutesCache().stream();
        Set<IRoute> routes;
        if (comparator == null) {
            routes = stream.filter(predicates).collect(Collectors.toCollection(supplier));
        } else {
            routes = stream.sorted(comparator).filter(predicates).collect(Collectors.toCollection(supplier));
        }

        duration = System.currentTimeMillis() - duration;
        String durationStr = DurationFormatUtils.formatDurationWords(duration, true, true);
        logger.trace(I18N.msg(MsgKey.GEN_METHOD_TOOK), "getResults", duration, durationStr);
        logger.trace(I18N.msg(MsgKey.GEN_LEAVING) + " getResults(): {}", routes);
        return routes;
    }

    @Override
    public IRouteQuery operator(LOGICAL_OPERATOR operator) {
        this.operator = operator;
        return this;
    }

    @Override
    public LOGICAL_OPERATOR getOperator() {
        return operator;
    }
}
