package com.armacsys.demo.data;

/**
 * Created by teveritt on 06/11/2014.
 */

import com.armacsys.demo.data.spi.IServiceProvider;
import org.kordamp.jipsy.ServiceProviderFor;

@ServiceProviderFor(IServiceProvider.class)
public class PoiServiceProviderImpl implements IServiceProvider {
    @Override
    public IDataSource createDataSource() {
        return new PoiDataSourceImpl();
    }

}
