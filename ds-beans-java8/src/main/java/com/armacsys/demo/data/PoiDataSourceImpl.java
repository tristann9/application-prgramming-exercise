package com.armacsys.demo.data;

import com.armacsys.demo.exceptions.DemoFeatureNotImplementedException;
import com.armacsys.demo.lib.i18n.I18N;
import com.armacsys.demo.lib.i18n.MsgKey;
import com.armacsys.demo.model.ILocation;
import com.armacsys.demo.model.IRoute;
import com.armacsys.demo.model.LocationBeanImpl;
import com.armacsys.demo.model.RouteBeanImpl;
import com.armacsys.demo.util.DijkstraModifiedSearch;
import com.armacsys.demo.util.IGraphSearch;
import com.armacsys.demo.util.ILocationQuery;
import com.armacsys.demo.util.IRouteQuery;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by teveritt on 06/11/2014.
 */

public class PoiDataSourceImpl implements IDataSource {
    private static final Logger logger = LoggerFactory
            .getLogger(PoiDataSourceImpl.class);
    private final String property = "demo.excel.file";
    private Set<ILocation> locationsCache = null;
    private Set<ILocation> orphansCache = null;
    private Set<IRoute> routesCache = null;

    PoiDataSourceImpl() {
        if (StringUtils.isBlank(System.getProperty(property))) {
            File defaultFile = new File("src/main/resources/Database.xls");
            if (defaultFile.exists() && defaultFile.isFile()) {
                setExcelFile(defaultFile); // The default (debugging)
            }
        }
    }

    @Override
    public void createLocation(ILocation Location) throws DemoFeatureNotImplementedException {
        throw new DemoFeatureNotImplementedException();
    }

    @Override
    public void deleteLocation(ILocation Location) throws DemoFeatureNotImplementedException {
        throw new DemoFeatureNotImplementedException();
    }

    @Override
    public void createRoute(IRoute route) throws DemoFeatureNotImplementedException {
        throw new DemoFeatureNotImplementedException();
    }

    @Override
    public void deleteRoute(IRoute route) throws DemoFeatureNotImplementedException {
        throw new DemoFeatureNotImplementedException();
    }

    @Override
    public ILocationQuery createLocationQuery() {
        return new LocationQueryImpl(this);
    }

    @Override
    public IRouteQuery createRouteQuery() {
        return new RouteQueryImpl(this);
    }

    @Override
    public IGraphSearch createGraphSearch() {
        return new DijkstraModifiedSearch(this);
    }

    public File getExcelFile() {
        return new File(System.getProperty(property));
    }

    public void setExcelFile(File excelFile) {
        if (excelFile == null) {
            throw new IllegalArgumentException("Parameter [excelFile] cannot be null");
        } else if (!excelFile.exists() || !excelFile.isFile()) {
            throw new IllegalArgumentException(String.format("Provided file is invalid: %s", excelFile.getAbsolutePath()));
        }

        System.setProperty(property, excelFile.getAbsolutePath());
        clearCaches();

    }

    Set<ILocation> getLocationsCache() {
        if (locationsCache == null) {
            loadExcelFile();
        }

        return locationsCache;
    }

    Set<IRoute> getRoutesCache() {
        if (routesCache == null) {
            loadExcelFile();
        }

        return routesCache;
    }

    Set<ILocation> getOrphansCache() {
        if (orphansCache == null) {
            loadExcelFile();
        }
        return orphansCache;
    }

    public void clearCaches() {
        locationsCache = null;
        routesCache = null;
        orphansCache = null;
        loadExcelFile();
    }

    void loadExcelFile() {
        logger.trace(I18N.msg(MsgKey.GEN_ENTERING) + " loadExcelFile()");
        long duration = System.currentTimeMillis();

        // Check for each one instead of clearing them all
        // by creating a new instance.
        // Later in the call if there is data
        // then it clears the caches.
        if (locationsCache == null) {
            locationsCache = new TreeSet<>();
        }

        if (routesCache == null) {
            routesCache = new TreeSet<>();
        }

        if (orphansCache == null) {
            orphansCache = new TreeSet<>();
        }

        File excelFile = new File(System.getProperty(property));
        try {


            logger.info(String.format("Loading Excel file: %s", excelFile.getAbsolutePath()));
            //Get the workbook instance for XLS or XLSX files
            Workbook workbook = WorkbookFactory.create(excelFile);

            //Get first sheet from the workbook since there is only one.
            Sheet sheet = workbook.getSheetAt(0);

            logger.info(String.format("Starting to process the Excel file: %s", excelFile.getAbsolutePath()));
            for (Row row : sheet) {
                String fromValue = row.getCell(0) != null ? row.getCell(0).getStringCellValue() : "";
                // If it contains From then it means it's the first row so skip it
                if (StringUtils.equals(fromValue, "From")) {
                    continue;
                }
                String toValue = row.getCell(1) != null ? row.getCell(1).getStringCellValue() : "";
                Double doubleVal = row.getCell(2) != null ? row.getCell(2).getNumericCellValue() : -1;
                Integer transitTimeValue = doubleVal.intValue();

                ILocation fromLocation = null;
                ILocation toLocation = null;

                if (StringUtils.isNotBlank(fromValue)) {
                    fromLocation = new LocationBeanImpl(fromValue);
                    locationsCache.add(fromLocation);
                    orphansCache.add(fromLocation);
                }

                if (StringUtils.isNotBlank(toValue)) {
                    toLocation = new LocationBeanImpl(toValue);
                    locationsCache.add(toLocation);
                    orphansCache.add(toLocation);
                }

                // Add valid routes where the from,to are not null
                // And the transit time is a positive integer
                if (fromLocation != null && toLocation != null && transitTimeValue > 0) {
                    IRoute route = new RouteBeanImpl(fromLocation, toLocation);
                    route.setTransitTime(transitTimeValue);

                    routesCache.add(route);
                }

            }

            // Find what locations are ophans
            for(IRoute route : routesCache) {
                orphansCache.remove(route.getFromLocation());
                orphansCache.remove(route.getToLocation());
            }

            // make the caches immutable
            locationsCache = Collections.unmodifiableSet(locationsCache);
            routesCache = Collections.unmodifiableSet(routesCache);
            orphansCache = Collections.unmodifiableSet(orphansCache);

        } catch (InvalidFormatException | IOException e) {
            logger.error("Problems reading the Excel file {}", excelFile.getAbsolutePath(), e);
        } finally {
            // Nothing to close.  No input streams used.
        }

        duration = System.currentTimeMillis() - duration;
        String durationStr = DurationFormatUtils.formatDurationWords(duration, true, true);

        logger.trace(I18N.msg(MsgKey.GEN_METHOD_TOOK), "loadExcelFile", duration, durationStr);
        logger.trace(I18N.msg(MsgKey.GEN_LEAVING) + " loadExcelFile()");
    }
}
