package com.armacsys.demo.model;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class LocationBeanImplTest {
    private String testName = null;
    private Integer testId = null;

    @Before
    public void setUp() throws Exception {
        testName = "Test Name";
        testId = 3;
    }

    @Test
    public void testGetId() throws Exception {
        ILocation location = new LocationBeanImpl(testId, testName);
        assertTrue(testId.equals(location.getId()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetId_negative() throws Exception {
        new LocationBeanImpl().setId(-1);
        new LocationBeanImpl(-1, testName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetId_nil() throws Exception {
        new LocationBeanImpl().setId(0);
        new LocationBeanImpl(0, testName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetId_null() throws Exception {
        new LocationBeanImpl().setId(null);
        new LocationBeanImpl(null, testName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetId_change_id() throws Exception {
        ILocation location = new LocationBeanImpl(testId, testName);
        location.setId(testId + testId);
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals(testName, new LocationBeanImpl(testName).getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetName_empty() throws Exception {
        new LocationBeanImpl().setName("");
        new LocationBeanImpl(testId, "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetName_null() throws Exception {
        new LocationBeanImpl().setName(null);
        new LocationBeanImpl(testId, null);
    }

    @Test
    public void testEqualsContract() {
        EqualsVerifier.forClass(LocationBeanImpl.class).suppress(Warning.NONFINAL_FIELDS).verify();
    }

    @Test
    public void testEquals_ids() {
        ILocation loc1 = new LocationBeanImpl(testId, testName);
        ILocation loc2 = new LocationBeanImpl(testId + testId, testName);
        assertFalse(loc1.equals(loc2));
    }

    @Test
    public void testEquals_name() {
        ILocation loc1 = new LocationBeanImpl(testId, testName);
        ILocation loc2 = new LocationBeanImpl(testId, testName + testName);
        assertFalse(loc1.equals(loc2));
    }

    @Test
    public void testSet() {
        Set<ILocation> set = new HashSet<>();
        set.add(new LocationBeanImpl(testId, testName));
        set.add(new LocationBeanImpl(testId, testName));
        set.add(new LocationBeanImpl(testId + testId, testName + testName));
        set.add(new LocationBeanImpl(testId + testId, testName + testName));
        set.add(new LocationBeanImpl(testId + testId + testId, testName + testName + testName));
        assertTrue(3 == set.size());
    }


    @Test
    public void testToString() throws Exception {

        String expected =
                String.format("%s{" +
                        "id='%s', name='%s'}", LocationBeanImpl.class.getSimpleName(), testId, testName);

        assertEquals(expected, new LocationBeanImpl(testId, testName).toString());
    }

    @Test
    public void testCompareTo() throws Exception {
        ILocation loc1 = new LocationBeanImpl(testId, testName);
        ILocation loc2 = new LocationBeanImpl(testId + testId, testName);
        ILocation loc3 = new LocationBeanImpl(testId, testName);
        ILocation loc4 = new LocationBeanImpl(testId, testName + testName);

        assertEquals(-1, loc1.compareTo(loc2));
        assertEquals(0, loc1.compareTo(loc3));
        assertTrue(0 > loc1.compareTo(loc4));
    }
}