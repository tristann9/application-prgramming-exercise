package com.armacsys.demo.model;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class GenerateUniqueIdTest {


    @Test(expected = IllegalArgumentException.class)
    public void testNull() throws Exception {
        AbstractBean b = new AbstractBean() {
        };
        b.generateUniqueId(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmpty() throws Exception {
        AbstractBean b = new AbstractBean() {
        };
        b.generateUniqueId("");
    }

    @Test
    public void testNameCollision()  throws Exception {
        String[] locations = {"Dublin", "Waterford", "London", "Zurich", "Paris"};
        // Any duplicates will be removed by the set
        Set<Integer> ids = new HashSet<>();
        for (String location : locations) {
            AbstractBean b = new AbstractBean() {
            };
            ids.add(b.generateUniqueId(location));
        }

        assertEquals(locations.length, ids.size());
    }
}