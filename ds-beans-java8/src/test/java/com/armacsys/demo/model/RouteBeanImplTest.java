package com.armacsys.demo.model;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RouteBeanImplTest {

    private ILocation testFrom = null;
    private ILocation testTo = null;
    private Integer testId = null;
    private Integer testTransitTime = null;

    @Before
    public void setUp() throws Exception {
        testFrom = new LocationBeanImpl("Dublin");
        testTo = new LocationBeanImpl("London");
        testId = 3;
        testTransitTime = 1;
    }

    @Test
    public void testGetFromLocation() throws Exception {
        IRoute route = new RouteBeanImpl();
        route.setFromLocation(testFrom);
        assertEquals(testFrom, route.getFromLocation());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetFromLocation_null() throws Exception {
        new RouteBeanImpl().setFromLocation(null);
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(RouteBeanImpl.class).suppress(Warning.NONFINAL_FIELDS).verify();
    }

    @Test
    public void testToString() throws Exception {
        String str = "%s{id='%s', from='%s', to='%s', transitTime='%s'}";
        String expected =
                String.format(str, RouteBeanImpl.class.getSimpleName(), testId, testFrom, testTo, testTransitTime);

        IRoute route = new RouteBeanImpl(testId, testFrom, testTo);
        route.setTransitTime(testTransitTime);
        assertEquals(expected, route.toString());
    }

    @Test
    public void testGetToLocation() throws Exception {
        IRoute route = new RouteBeanImpl();
        route.setToLocation(testTo);
        assertEquals(testTo, route.getToLocation());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetToLocation_null() throws Exception {
        new RouteBeanImpl().setToLocation(null);
    }

    @Test
    public void testGetId() throws Exception {
        IRoute route = new RouteBeanImpl(testId, testFrom, testTo);
        assertTrue(testId.equals(route.getId()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetId_negative() throws Exception {
        new RouteBeanImpl().setId(-1);
        new RouteBeanImpl(-1, testFrom, testTo);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetId_nil() throws Exception {
        new RouteBeanImpl().setId(0);
        new RouteBeanImpl(0, testFrom, testTo);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetId_null() throws Exception {
        new RouteBeanImpl().setId(null);
        new RouteBeanImpl(null, testFrom, testTo);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetId_change_id() throws Exception {
        IRoute route = new RouteBeanImpl(testId, testFrom, testTo);
        route.setId(testId + testId);
    }

    @Test
    public void testGetTransitTime() throws Exception {
        IRoute route = new RouteBeanImpl();
        route.setTransitTime(testTransitTime);
        assertEquals(testTransitTime, route.getTransitTime());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetTransitTime_null() throws Exception {
        new RouteBeanImpl().setTransitTime(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetTransitTime_nil() throws Exception {
        new RouteBeanImpl().setTransitTime(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetTransitTime_negative() throws Exception {
        new RouteBeanImpl().setTransitTime(-1);
    }

    @Test
    public void testCompareTo() throws Exception {

    }

    @Test
    public void testSet() {
        ILocation loc1 = new LocationBeanImpl("Waterford");
        ILocation loc2 = new LocationBeanImpl("Paris");

        Set<IRoute> set = new HashSet<>();
        set.add(new RouteBeanImpl(testId, testFrom, testTo));
        set.add(new RouteBeanImpl(testId, testFrom, testTo));
        set.add(new RouteBeanImpl(testId + testId, testFrom, loc1));
        set.add(new RouteBeanImpl(testId + testId, testFrom, loc1));
        set.add(new RouteBeanImpl(testId + testId + testId, testFrom, loc2));
        assertTrue(3 == set.size());
    }
}