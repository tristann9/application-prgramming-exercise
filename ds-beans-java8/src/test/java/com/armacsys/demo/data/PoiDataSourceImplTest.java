package com.armacsys.demo.data;

import com.armacsys.demo.exceptions.DemoFeatureNotImplementedException;
import com.armacsys.demo.model.ILocation;
import com.armacsys.demo.model.IRoute;
import org.junit.Test;

import java.io.File;

import static org.mockito.Mockito.mock;

public class PoiDataSourceImplTest {

    @Test(expected = DemoFeatureNotImplementedException.class)
    public void testCreateLocation() throws Exception {
        new PoiDataSourceImpl().createLocation(mock(ILocation.class));
    }

    @Test(expected = DemoFeatureNotImplementedException.class)
    public void testDeleteLocation() throws Exception {
        new PoiDataSourceImpl().deleteLocation(mock(ILocation.class));
    }

    @Test(expected = DemoFeatureNotImplementedException.class)
    public void testCreateRoute() throws Exception {
        new PoiDataSourceImpl().createRoute(mock(IRoute.class));
    }

    @Test(expected = DemoFeatureNotImplementedException.class)
    public void testDeleteRoute() throws Exception {
        new PoiDataSourceImpl().deleteRoute(mock(IRoute.class));
    }

    @Test
    public void testCreateLocationQuery() throws Exception {

    }


    @Test(expected = IllegalArgumentException.class)
    public void testSetExcelFile_null() throws Exception {
        new PoiDataSourceImpl().setExcelFile(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetExcelFile_file_not_exists() throws Exception {
        new PoiDataSourceImpl().setExcelFile(new File("SomeRandomFileName.tmp"));
    }

}