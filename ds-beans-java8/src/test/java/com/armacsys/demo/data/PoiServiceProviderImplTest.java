package com.armacsys.demo.data;

import com.armacsys.demo.data.spi.IServiceProvider;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PoiServiceProviderImplTest {

    @Test
    public void testCreateDataSource() throws Exception {

        IServiceProvider sp = mock(IServiceProvider.class);
        IDataSource ds = mock(PoiDataSourceImpl.class);
        when(sp.createDataSource()).thenReturn(ds);

        assertTrue(sp.createDataSource() instanceof PoiDataSourceImpl);
    }


}