package com.armacsys.demo;

import com.armacsys.demo.data.IDataSource;
import com.armacsys.demo.exceptions.DemoFeatureNotImplementedException;
import com.armacsys.demo.model.ILocation;
import com.armacsys.demo.model.IRoute;
import com.armacsys.demo.util.IGraphSearch;
import com.armacsys.demo.util.ILocationQuery;
import com.armacsys.demo.util.IRouteQuery;
import org.kordamp.jipsy.ServiceProviderFor;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by teveritt on 05/11/2014.
 */

@ServiceProviderFor(IDataSource.class)
public class JPADataSourceImpl implements IDataSource {

    @PersistenceContext(unitName = "excel-demo")
    EntityManager mgr;


    /**
     * Creates a Location
     *
     * @param location -- the location bean/entity to create
     */
    @Override
    public void createLocation(ILocation location) throws DemoFeatureNotImplementedException {

    }

    /**
     * Deletes a Location
     *
     * @param location -- the location bean/entity to delete
     */
    @Override
    public void deleteLocation(ILocation location) throws DemoFeatureNotImplementedException {

    }

    /**
     * Creates a Route
     *
     * @param route -- the route bean/entity to create
     */
    public void createRoute(IRoute route) throws DemoFeatureNotImplementedException {
        throw new DemoFeatureNotImplementedException();
    }

    /**
     * Deletes a Route -- the route bean/entity to delete
     *
     * @param route
     */
    public void deleteRoute(IRoute route) throws DemoFeatureNotImplementedException {
        throw new DemoFeatureNotImplementedException();
    }

    /**
     * Create a Location Query to query the datasource for locations
     *
     * @return ILocationQuery query
     */
    @Override
    public ILocationQuery createLocationQuery() {
        return null;
    }

    /**
     * Create a Route Query to query the datasource for routes
     *
     * @return IRouteQuery query
     */
    @Override
    public IRouteQuery createRouteQuery() {
        return null;
    }

    /**
     * Create a Graph Search to query two locations in a set
     * of locations and routes between them.
     *
     * @return IRouteQuery query
     */
    @Override
    public IGraphSearch createGraphSearch() {
        return null;
    }
}
