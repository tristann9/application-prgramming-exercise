package com.armacsys.demo;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by teveritt on 05/11/2014.
 */
public final class EMFactory {

    private static final EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("excel-demo");

    private EMFactory() {
    }

    public static EntityManagerFactory get() {
        return emf;
    }
}
