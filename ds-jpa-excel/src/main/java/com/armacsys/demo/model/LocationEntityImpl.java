package com.armacsys.demo.model;

import com.armacsys.demo.exceptions.BlankNameException;
import com.armacsys.demo.exceptions.IdAlreadySetException;
import com.armacsys.demo.exceptions.IdInvalidException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;

/**
 * Created by teveritt on November 09, 2014.
 */

@Entity
@Table(name = "Location")
public class LocationEntityImpl implements ILocation {
    private static final long serialVersionUID = 3403445247523610181L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Basic
    @Column(name = "Name")
    private String name = null;

    // Perhaps do some joins here.
    // Example:
    // @OneToMany
    // Location ----E Route

    public LocationEntityImpl() {

    }

    public LocationEntityImpl(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public LocationEntityImpl(String name) {
        this.name = name;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        if (this.id != null && !this.id.equals(id)) {
            throw new IdAlreadySetException();
        } else if (id == null || id <= 0) {
            throw new IdInvalidException();
        }


        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        if (StringUtils.isBlank(name)) {
            throw new BlankNameException();
        }
        this.name = name;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocationEntityImpl)) return false;

        LocationEntityImpl that = (LocationEntityImpl) o;

        return new EqualsBuilder().append(this.getId(), that.getId()).append(this.getName(), that.getName())
                .isEquals();
    }

    @Override
    public final int hashCode() {

        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
                append(getId()).append(getName()).toHashCode();
    }

    @Override
    public String toString() {
        return String.format("%s{" +
                "id='%s', name='%s'}", LocationEntityImpl.class.getSimpleName(), id, name);
    }

    @Override
    public int compareTo(ILocation that) {
        int name = this.getName().compareTo(that.getName());
        return name == 0 ? this.getId().compareTo(that.getId()) : name;
    }
}
