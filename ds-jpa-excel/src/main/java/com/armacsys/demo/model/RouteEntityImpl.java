package com.armacsys.demo.model;

import com.armacsys.demo.exceptions.IdAlreadySetException;
import com.armacsys.demo.exceptions.IdInvalidException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;

/**
 * Entity class or POJO representing a Route
 * <p>
 * Created by teveritt on 05/11/2014.
 */

@Entity
@Table(name = "Route")
public class RouteEntityImpl implements IRoute {

    private static final long serialVersionUID = -5368877927619014529L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Basic
    @Column(name = "From")
    private ILocation from;

    @Basic
    @Column(name = "To")
    private ILocation to;

    @Basic
    @Column(name = "TransitTime")
    private Integer transitTime;

    public RouteEntityImpl() {

    }

    public RouteEntityImpl(Integer id, ILocation from, ILocation to) {
        this.id = id;
        this.from = from;
        this.to = to;
    }

    public RouteEntityImpl(ILocation from, ILocation to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public ILocation getFromLocation() {
        return from;
    }

    @Override
    public void setFromLocation(ILocation from) {
        if (from == null) {
            throw new IllegalArgumentException("From location cannot be null");
        }
        this.from = from;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RouteEntityImpl)) return false;

        RouteEntityImpl that = (RouteEntityImpl) o;

        return new EqualsBuilder().append(this.getId(), that.getId())
                .append(this.getFromLocation(), that.getFromLocation())
                .append(this.getToLocation(), that.getToLocation())
                .isEquals();
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
                append(getId()).append(getFromLocation()).append(getToLocation()).toHashCode();
    }

    @Override
    public String toString() {
        String str = "%s{id='%s', from='%s', to='%s', transitTime='%s'}";
        return String.format(str, RouteEntityImpl.class.getSimpleName(), id, from, to, transitTime);
    }

    @Override
    public ILocation getToLocation() {
        return to;
    }

    @Override
    public void setToLocation(ILocation to) {
        if (to == null) {
            throw new IllegalArgumentException("To location cannot be null");
        }
        this.to = to;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        if (this.id != null && !this.id.equals(id)) {
            throw new IdAlreadySetException();
        } else if (id == null || id <= 0) {
            throw new IdInvalidException();
        }
        this.id = id;
    }

    @Override
    public Integer getTransitTime() {
        return transitTime;
    }

    /**
     * @param transitTime
     * @throws java.lang.IllegalArgumentException if the transitTime is not a valid positive integer
     */
    @Override
    public void setTransitTime(Integer transitTime) {
        if (transitTime == null || !(transitTime > 0)) {
            throw new IllegalArgumentException("transitTime needs to be a positive integer.");
        }

        this.transitTime = transitTime;
    }

    @Override
    public int compareTo(IRoute that) {
        String thisName = String.format("%s:%s", this.getFromLocation(), this.getToLocation());
        String thatName = String.format("%s:%s", that.getFromLocation(), that.getToLocation());
        int name = thisName.compareTo(thatName);
        return name == 0 ? this.getId().compareTo(that.getId()) : name;
    }
}
