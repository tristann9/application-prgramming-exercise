package com.armacsys.demo;

import com.armacsys.demo.data.PoiDataSourceImpl;
import com.armacsys.demo.model.ILocation;
import com.armacsys.demo.model.IRoute;
import com.armacsys.demo.model.LocationBeanImpl;
import com.armacsys.demo.util.ILocationQuery;
import com.armacsys.demo.util.IRouteQuery;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by teveritt on 05/11/2014.
 *
 * This is a simple, infinite looping application to showcase some API calls.
 * This class does not follow any formal design nor does it handle logging and internationalisation.
 */
public class Main {

    private final Scanner console;
    private final IExerciseApi api;

    private Main() {
        console = new Scanner(System.in);
        api = ExerciseApiFactory.get();
        help();
        waitForInput();
    }

    public static void main(String[] args) throws Exception {
        new Main();
    }

    private void printRoutes(Set<IRoute> routes) {
        if (routes.isEmpty()) {
            System.out.println("No routes found");
        } else {
            for (IRoute route : routes) {
                String from = route.getFromLocation().getName();
                String to = route.getToLocation().getName();
                String str = String.format("%s --> %s (transitTime=%s)", from, to, route.getTransitTime());
                System.out.println(str);
            }
        }

    }

    private void waitForInput() {
        try {
            System.out.print(">> ");

            String input = console.nextLine();
            if (StringUtils.isBlank(input)) {
                System.err.println("ERR: Please input an option");
            } else {
                input = StringUtils.trim(input);
                if (input.split(" ").length == 1) {
                    try {
                        getClass().getMethod(input).invoke(this);
                    } catch (Exception e) {
                        System.err.println("ERR: Invalid option");
                    }
                }
            }

            waitForInput();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void quit() {
        console.close();
        System.exit(0);
    }

    public void help() {
        System.out.println("Options:\n");

        Method[] methods = getClass().getMethods();
        int optionCount = 1;
        String[] omit = {"wait", "toString", "hashCode", "getClass", "notify", "notifyAll"};
        for (Method method : methods) {
            String name = method.getName();
            if (method.getParameterCount() == 0 && !ArrayUtils.contains(omit, name)) {
                System.out.println(String.format("\t%s) %s", optionCount, name));
                optionCount += 1;
            }
        }
    }

    public void locations() {
        ILocationQuery query = api.createLocationQuery();
        Set<ILocation> locations = query.orderBy(ILocation.PROPERTY.NAME, true).getResults();

        if (locations.isEmpty()) {
            System.out.println("No locations found");
        } else {
            for (ILocation location : locations) {
                System.out.println(location.getName());
            }
        }

        waitForInput();
    }

    public void locations_reverse() {
        ILocationQuery query = api.createLocationQuery();
        Set<ILocation> locations = query.orderBy(ILocation.PROPERTY.NAME, false).getResults();

        if (locations.isEmpty()) {
            System.out.println("No locations found");
        } else {
            for (ILocation location : locations) {
                System.out.println(location.getName());
            }
        }

        waitForInput();
    }

    public void isolated_locations() {
        ILocationQuery query = api.createLocationQuery();

        Set<ILocation> locations = query.setIsolated(true)
                .orderBy(ILocation.PROPERTY.NAME, true)
                .getResults();

        if (locations.isEmpty()) {
            System.out.println("No isolated locations found");
        } else {
            for (ILocation location : locations) {
                System.out.println(location.getName());
            }
        }


        waitForInput();
    }

    public void routes() {
        IRouteQuery query = api.createRouteQuery();
        printRoutes(query.getResults());
        waitForInput();
    }

    public void neighbours_out() {
        System.out.println("Enter a location (case sensitive) to find all it's outgoing neighbours");

        String input = console.nextLine();
        if (StringUtils.isBlank(input)) {
            System.err.println("ERR: No location was entered");
        } else {
            IRouteQuery query = api.createRouteQuery();
            printRoutes(query.setFromLocation(new LocationBeanImpl(input)).getResults());
        }

        waitForInput();
    }

    public void neighbours_in() {
        System.out.println("Enter a location (case sensitive) to find all it's incoming neighbours");

        String input = console.nextLine();
        if (StringUtils.isBlank(input)) {
            System.err.println("ERR: No location was entered");
        } else {
            IRouteQuery query = api.createRouteQuery();
            printRoutes(query.setToLocation(new LocationBeanImpl(input)).getResults());
        }

        waitForInput();
    }

    public void find_route() {
        System.out.println("Enter the starting and finish locations (case sensitive) to find the quickest route");

        String input = console.nextLine();
        if (StringUtils.isBlank(input)) {
            System.err.println("ERR: No location was entered");
        } else if (input.split(" ").length != 2) {
            System.err.println("ERR: Two locations are needed");
        } else {
            String[] locations = input.split(" ");
            ILocation start = new LocationBeanImpl(locations[0]);
            ILocation end = new LocationBeanImpl(locations[1]);

            IRouteQuery query = api.createRouteQuery();
            List<IRoute> routes = query.getRoutesBetween(start, end);
            int totTransitTime = 0;
            StringBuilder builder = new StringBuilder();
            builder.append(String.format("Routes found between %s to %s: %s", start.getName(), end.getName(), routes.size()));
            builder.append("\n");
            for (IRoute route : routes) {
                ILocation from = route.getFromLocation();
                ILocation to = route.getToLocation();
                int transitTime = route.getTransitTime();
                totTransitTime += transitTime;

                if (start.equals(from)) {
                    builder.append(start.getName());
                    builder.append(String.format(" --[%s]--> %s", transitTime, to.getName()));
                } else {
                    builder.append(String.format(" --[%s]--> %s", transitTime, to.getName()));
                }

            }

            System.out.println(builder.toString());

            if (totTransitTime > 0) {
                System.out.println(String.format("Total Transit Time: %s", totTransitTime));
            }
        }

        waitForInput();
    }

    public void set_excel_file_path() {
        System.out.println("Set the absolute path (case sensitive) to the excel file to use as a database.");
        System.out.println("\t NOTE: The first row contains the three column names: From, To, Transit Time");

        String input = console.nextLine();
        if (StringUtils.isBlank(input)) {
            System.err.println("ERR: No location was entered");
        } else {
            File file = new File(input);
            if (!file.exists() || !file.isFile()) {
                System.err.println("ERR: File is invalid or does not exist");
            } else {
                if (api.getDataSource() instanceof PoiDataSourceImpl) {
                    ((PoiDataSourceImpl) api.getDataSource()).setExcelFile(file);
                    System.out.println("File set!");
                }
            }
        }
    }
}
